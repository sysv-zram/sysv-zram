# sysv-zram (Português do Brasil)

Script do ZRAM para o SysVint.

## Instalação
1 - Clone esse repositório e depois altere o diretório

2 - Copie o arquivo zram para o /etc/init.d/

3 - Defina o script para ser iniciado nos níveis 2 3 4 5.

## Usage
service zram (start | stop | status)


#### License
GPL Version 3 (GPLv3)
